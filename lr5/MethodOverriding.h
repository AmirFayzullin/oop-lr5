#include <iostream>
#pragma once
using namespace std;

namespace MethodOverriding {
	class Base {
	public:
		virtual void someMethod() {
			cout << "Base::someMethod()\n";
		}

		void anotherMethod() {
			cout << "Base::anotherMethod()\n";
			virtualMethod();
		}

		virtual void virtualMethod() {
			cout << "Base::virtualMethod()\n";
		}

		virtual ~Base() {						// virtual destructor is needed when we delete child object, which is stored in a variable 
			cout << "Base::~Base()\n";			// of a base class to call the overriding destructor of a child and then, overriden base destructor
		}
	};

	class Child : public Base {
	public:
		void someMethod() override {
			cout << "Child::someMethod()\n";
		}

		void virtualMethod() override {
			cout << "Child::virtualMethod()\n";
		}

		~Child() override {
			cout << "Child::~Child()\n";
		}
	};

	void showDebug() {
		Base* inBase = new Child();	// 1) child destructor, 2) base destructor will be called 
		Child child;				// 1) child destructor, 2) base destructor will be called 
		Base base;					// base destructor will be called

		inBase->someMethod();		// child method invocation
		child.someMethod();			// child method invocation
		base.someMethod();			// base method invocation

		child.anotherMethod();		// base method called -> overriding virtualMethod of a child called

		delete inBase;						
	}

	void run() {
		cout << "METHOD OVERRIDING START\n";
		showDebug();
		cout << "METHOD REDEFINITION END\n\n\n\n";
	}
}
