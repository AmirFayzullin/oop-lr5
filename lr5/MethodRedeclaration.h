#include <iostream>
#pragma once
using namespace std;

namespace MethodRedeclaration {
	class Base {
	public:
		void someMethod() {
			cout << "Base::someMethod()\n";
		}

		void anotherMethod() {
			cout << "Base::anotherMethod()\n";
			nonVirtualMethod();
		}

		void nonVirtualMethod() {
			cout << "Base::nonVirtualMethod()\n";
		}

		~Base() {
			cout << "Base::~Base()\n";
		}
	};

	class Child : public Base {
	public:
		void someMethod() {
			cout << "Child::someMethod()\n";
		}

		void nonVirtualMethod() {
			cout << "Child::nonVirtualMethod()\n";
		}

		~Child() {
			cout << "Child::~Child()\n";
		}
	};

	void showDebug() {
		Base* inBase = new Child();		// only base destructor will be called
		Child inChild;					// 1) child destructor, 2) base destructor will be called

		inBase->someMethod();			// base method called
		inChild.someMethod();			// child method called
		((Child*)inBase)->someMethod();	// child method called

		inChild.anotherMethod();		// base method called -> base non-virtual method os called from "anotherMethod"

		delete inBase;					
	}

	void run() {
		cout << "METHOD REDECLARATION START\n";
		showDebug();
		cout << "METHOD REDECLARATION END\n\n\n\n";
	}
}