#include <cstdio>
#include <memory>
#pragma once
using namespace std;

namespace ObjectsLifecycle {
	class Base {
	protected:
		int value;
	public:
		Base(int v) : value(v) {
			printf("Base::Base(%d)\n", value);
		}

		virtual void someMethod() {
			printf("Base::someMethod(), value = %d\n", value);
		}

		virtual ~Base() {
			printf("Base::~Base(), value = %d\n", value);
		}
	};

	class Child : public Base {
	public:
		Child(int v) : Base(v) {
			printf("Child::Child(%d)\n", value);
		}

		void someMethod() override {
			printf("Child::someMethod(), value = %d\n", value);
		}

		~Child() override {
			printf("Child::~Child(), value = %d\n", value);
		}
	};

	void getByCopy(Base c) {
		printf("\ngetByCopy()\n");
		c.someMethod();
	}	// c is destroyed here as Base class
	
	void getByPointer(Base* c) {
		printf("\ngetByPointer()\n");
		c->someMethod();
	}	// c is alive, beacuse we have a pointer to the field of dynamic memory 


	void getByRef(Base& c) {
		printf("\ngetByRef()\n");
		c.someMethod();
	}	// c is alive, because we work with the reference

	Base returnByCopy() {
		printf("\nreturnByCopy()\n");
		Child c(20);		// c is created
		return c;
	}	// c is destroyed here

	Base* returnByPointer() {
		printf("\nreturnByPointer()\n");
		Child* c = new Child(25);		// instance is created and allocated in heap
		return c;				
	}	// c is alive here

	Base& returnByRef() {
		printf("\nreturnByRef()\n");
		Child c(30);					// c is created
		return c;
	}	// c is destroyed, outer world will get the trush

	void showDebug() {
		Base c1(10);
		Base* c2 = new Child(15);
		getByCopy(c1);				// passed as Base		
		getByPointer(c2);
		getByRef(c1);				// passed as Base

		Base c3 = returnByCopy();
		c3.someMethod();

		Base* c4 = returnByPointer();
		c4->someMethod();

		Base& c5 = returnByRef();
		c5.someMethod();

		printf("\n\n");

		delete c2;
		delete c4;
	}

	unique_ptr<Child> testUniquePtr(unique_ptr<Child> c) {
		printf("testUniquePtr()\n");
		(*c).someMethod();
		return c;
	}

	shared_ptr<Child> testSharedPtr(shared_ptr<Child> c) {
		printf("testSharedPtr()\n");
		(*c).someMethod();
		return c;
	}

	void showSmartPointersDebug() {
		printf("\n\nSMART POINTERS\n");
		// the feature of unique_ptr is a single owner of a memory
		unique_ptr<Child> c1(new Child(25));
		c1 = testUniquePtr(move(c1));	// ownerhisp is given to function parameter and returned to c1
		testUniquePtr(move(c1));		// ownership is given to function parameter and returned to nothing. Object will be deleted

		// the feature of shared_ptr is tracking of references to the object
		// while there is at least one reference, object exists
		// but when number of references = 0, object is deleted
		shared_ptr<Child> c2(new Child(30));			// 1 ref
		testSharedPtr(c2);							// 2 refs in function
		{
			shared_ptr<Child> c2ptrCopy(c2);		// 2 refs
			(*c2ptrCopy).someMethod();
			printf("debug line 1\n");
		}											// again 1 ref
		printf("debug line 2\n");
	}												// 0 refs, object is deleted

	void run() {
		printf("OBJECTS LIFECYCLE START\n");
		showDebug();
		showSmartPointersDebug();
		printf("OBJECTS LIFECYCLE END\n");
	}
}
