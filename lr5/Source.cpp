#include <iostream>
#include "MethodRedeclaration.h";
#include "MethodOverriding.h";
#include "TypeCasting.h";
#include "ObjectsLifecycle.h";
using namespace std;

int main() {
	MethodRedeclaration::run();
	MethodOverriding::run();
	TypeCasting::run();
	ObjectsLifecycle::run();
}