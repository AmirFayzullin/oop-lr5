#include <cstdio>
#include <string>
#pragma once
using namespace std;

namespace TypeCasting {
	class Point {
	protected:
		int x, y;
	public:
		Point(int _x, int _y) : x(_x), y(_y) {}

		virtual void draw() {
			printf("Point::shift()\n");
			printf("(%d, %d)\n", x, y);
		}

		virtual void shift(int dx, int dy) {
			x += dx;
			y += dy;
		}

		virtual string classname() { return "Point"; }
		virtual bool isA(string who) { return who == "Point"; }
	};

	class Point3d : public Point {
	protected:
		int z;
	public:
		Point3d(int _x, int _y, int _z) : Point(_x, _y), z(_z) {}

		virtual void draw() override {
			printf("(%d, %d, %d)\n", x, y, z);
		}

		virtual void shift(int dx, int dy, int dz) {
			printf("Point3d::shift()\n");
			x += dx;
			y += dy;
			z += dz;
		}

		virtual string classname() override { return "Point3d"; }
		virtual bool isA(string who) { return who == "Point3d" || Point::isA(who); }
	};

	class ColoredPoint3d : public Point3d {
		string color;

	public:
		ColoredPoint3d(int _x, int _y, int _z, string _color) : 
			Point3d(_x, _y, _z), color(_color) {}

		void draw() override {
			printf("(%d, %d, %d) %s\n", x, y, z, color.c_str());
		}

		string classname() override { return "ColoredPoint3d"; }
		virtual bool isA(string who) { return who == "ColoredPoint3d" || Point3d::isA(who); }
	};

	void showDebug() {
		const int len = 3;
		Point* points[len] = { new Point(10, 20), new Point3d(15, 30, 45), new ColoredPoint3d(20, 40, 60, "red") };

		for (int i = 0; i < len; i++)
			points[i]->draw();

		/*for (int i = 0; i < len; i++)
			((Point3d*)points[i])->shift(-10, -10, -10);*/ //dangerous type casting, because points may contain objects, that couldn't be casted to Point3d

		printf("\nclassname typecsting\n");
		for (int i = 0; i < len; i++)
			if (points[i]->classname() == "Point3d") { 
				// will be true only for Point3d, for ColoredPoint3d classname will return "ColoredPoint3d"
				// this is the problem of such approach for type checking
				Point3d* point = (Point3d*)points[i];
				point->shift(-5, -5, -5);
				point->draw();
			}

		printf("\nisA typecsting\n");
		for (int i = 0; i < len; i++) 
			if (points[i]->isA("Point3d")) {
				// this approach is much more useful, because the chain of isA method calls
				// though all of the ancestors will give us defined understanding of object's 
				// belonging to some particular class
				Point3d* point = (Point3d*)points[i];
				point->shift(-5, -5, -5);
				point->draw();
			}

		printf("\ndynamic_cast\n");
		for (int i = 0; i < len; i++) {
			Point3d* point = dynamic_cast<Point3d*>(points[i]);
			if (point == nullptr) continue;
			
			point->shift(-5, -5, -5);
			point->draw();
		}

		// with classname we cannot define if, for instance, ColoredPoint3d is a Point3d at the same time
		// isA gives opptorunity to fix it 


		for (int i = 0; i < len; i++) 
			delete points[i];
	}

	void run() {
		printf("TYPE CASTING START\n");
		showDebug();
		printf("TYPE CASTING END\n\n\n\n");
	}
}
